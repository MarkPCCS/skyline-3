# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.148');


# ---------------------------------------------------------------------- #
# Add table "manufacturer_service_provider"                              #
# ---------------------------------------------------------------------- #

CREATE TABLE `manufacturer_service_provider` (
	`ManufacturerID` INT(10) NOT NULL,
	`ServiceProviderID` INT(10) NOT NULL,
	`Authorised` ENUM('Yes','No') NULL DEFAULT 'Yes',
	`DataChecked` ENUM('Yes','No') NULL DEFAULT 'No',
	PRIMARY KEY (`ManufacturerID`, `ServiceProviderID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE service_provider ADD NumberOfVehicles INT(11) NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.149');