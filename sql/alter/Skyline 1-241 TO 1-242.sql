
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.241');


# ---------------------------------------------------------------------- #
# Modify table part_section                                              #
# ---------------------------------------------------------------------- #
ALTER TABLE `part_section_code`
	ALTER `PartSectionCode` DROP DEFAULT;
ALTER TABLE `part_section_code`
	CHANGE COLUMN `PartSectionCode` `PartSectionCode` CHAR(4) NOT NULL AFTER `PartSectionCodeID`;

# ---------------------------------------------------------------------- #
# Modify table part_repair_code                                          #
# ---------------------------------------------------------------------- #
ALTER TABLE `part_repair_code`
	ALTER `PartRepairCode` DROP DEFAULT;
ALTER TABLE `part_repair_code`
	CHANGE COLUMN `PartRepairCode` `PartRepairCode` CHAR(4) NOT NULL AFTER `PartRepairCodeID`;

# ---------------------------------------------------------------------- #
# Modify table part_defect_code                                          #
# ---------------------------------------------------------------------- #
ALTER TABLE `part_defect_code`
	ALTER `PartDefectCode` DROP DEFAULT;
ALTER TABLE `part_defect_code`
	CHANGE COLUMN `PartDefectCode` `PartDefectCode` CHAR(4) NOT NULL AFTER `PartDefectCodeID`;

# ---------------------------------------------------------------------- #
# Modify table symptom_code                                              #
# ---------------------------------------------------------------------- #
ALTER TABLE `symptom_code`
	ALTER `SymptomCode` DROP DEFAULT;
ALTER TABLE `symptom_code`
	CHANGE COLUMN `SymptomCode` `SymptomCode` CHAR(4) NOT NULL AFTER `SymptomCodeID`;

 
# ---------------------------------------------------------------------- #
# Insert Part Data                                                       #
# ---------------------------------------------------------------------- # 
INSERT IGNORE INTO `condition_code` (`ConditionCodeID`, `ConditionCode`, `ConditionCodeDescription`, `ConditionCodeComment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '1', 'Constantly', 'constant faults', 1, '2013-04-19 11:25:55'),
	(2, '2', 'Intermittently', 'intermittent faults', 1, '2013-04-19 11:25:55'),
	(3, 'A', 'Due to physical damage', 'fault was due to customer misuse', 1, '2013-04-19 11:25:55'),
	(4, 'C', 'Only on certain networks', 'is the fault only occuring on particular networks/services i.e. 3G', 1, '2013-04-19 11:25:55'),
	(5, 'H', 'In standby/idle mode', 'fault occurs when no fuctions/features are being activated', 1, '2013-04-19 11:25:55'),
	(6, 'L', 'Liquid contamination', 'fault was due to liquid damage', 1, '2013-04-19 11:25:55'),
	(7, 'Q', 'only while sending/writing', 'fault occurs when making calls or sending/writing SMS/MMS', 1, '2013-04-19 11:25:55'),
	(8, 'R', 'only while receiving/reading', 'fault occurs when answering call or receiving/reading SMS/MMS', 1, '2013-04-19 11:25:55');

INSERT IGNORE INTO `condition_code_subset` (`ConditionCodeID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 106, 11, 1, '2013-04-26 10:50:39'),
	(2, 106, 11, 1, '2013-04-26 10:50:39'),
	(3, 106, 11, 1, '2013-04-26 10:50:39'),
	(4, 106, 11, 1, '2013-04-26 10:50:39'),
	(5, 106, 11, 1, '2013-04-26 10:50:39'),
	(6, 106, 11, 1, '2013-04-26 10:50:39'),
	(7, 106, 11, 1, '2013-04-26 10:50:39'),
	(8, 106, 11, 1, '2013-04-26 10:50:39');

INSERT IGNORE INTO `part_defect_code` (`PartDefectCodeID`, `PartDefectCode`, `PartDefectCodeDescription`, `PartDefectCodeComment`, `ModifiedUserID`, `ModifiedDate`, `Status`) VALUES
	(1, '1', 'Software bug', 'fault with software already on handset', 1, '2013-04-26 10:21:56', 'Active'),
	(2, '4', 'No Fault Found', 'no fault could be found, customer mis-understanding/eductaion', 1, '2013-04-26 10:21:56', 'Active'),
	(3, '6', 'Unable to diagnose fault', 'use when forwarding a handset to a higher level repair centre', 1, '2013-04-26 10:21:56', 'Active'),
	(4, '9', 'Customer misuse', 'BER damage caused by the end user', 1, '2013-04-26 10:21:56', 'Active'),
	(5, 'A', 'Worn out', 'cosmetic related defects', 1, '2013-04-26 10:21:57', 'Active'),
	(6, 'G', 'Scratched', 'cosmetic related defects', 1, '2013-04-26 10:21:57', 'Active'),
	(7, 'N', 'Defective electrical component', 'defective solder device (i.e. SMD/BGA devices)', 1, '2013-04-26 10:21:57', 'Active'),
	(8, 'T', 'Bad contact connection', 'other soldered devices (i.e. SIM reader, connectors etc)', 1, '2013-04-26 10:21:57', 'Active'),
	(9, 'V', 'Cracked printed circuit board', 'visible defects in the PBA or FPCB', 1, '2013-04-26 10:21:57', 'Active'),
	(10, 'W4', 'Cold/no soldering/solder did not stick to the component', 'Reflow of any solder connections, no components replaced', 1, '2013-04-26 10:21:57', 'Active');

INSERT IGNORE INTO `part_defect_code_subset` (`PartDefectCodeID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 106, 11, 1, '2013-04-26 10:48:44'),
	(2, 106, 11, 1, '2013-04-26 10:48:44'),
	(3, 106, 11, 1, '2013-04-26 10:48:44'),
	(4, 106, 11, 1, '2013-04-26 10:48:44'),
	(5, 106, 11, 1, '2013-04-26 10:48:44'),
	(6, 106, 11, 1, '2013-04-26 10:48:44'),
	(7, 106, 11, 1, '2013-04-26 10:48:44'),
	(8, 106, 11, 1, '2013-04-26 10:48:44'),
	(9, 106, 11, 1, '2013-04-26 10:48:44'),
	(10, 106, 11, 1, '2013-04-26 10:48:44');

INSERT IGNORE INTO `part_repair_code` (`PartRepairCodeID`, `PartRepairCode`, `PartRepairCodeDescription`, `PartRepairCodeComment`, `ModifiedUserID`, `ModifiedDate`, `Status`) VALUES
	(1, '1', 'Boot Software correction/reset', 'to be used with JTAG reapirs only (no parts used for repair)', 1, '2013-04-26 10:27:00', 'Active'),
	(2, '2', 'Software upgrade', 'reload/upgrade handset firmware', 1, '2013-04-26 10:27:00', 'Active'),
	(3, '11', 'Formatted', 'Special code for specific SW upgrades', 1, '2013-04-26 10:27:00', 'Active'),
	(4, 'A', 'Replacement', 'replacement of any soldered parts', 1, '2013-04-26 10:27:00', 'Active'),
	(5, 'AF', 'Free of cost part', 'A free of charge part as supplied by the OEM was used', 1, '2013-04-26 10:27:00', 'Active'),
	(6, 'C', 'Electrical alignment', 'RF calibration of handset', 1, '2013-04-26 10:27:00', 'Active'),
	(7, 'C3', 'Firmware adjustment', 'JTAG use for Samsung', 1, '2013-04-26 10:27:00', 'Active'),
	(8, 'O', 'Refurbishing', 'replaced any cosmetic parts (i.e. front cover, keypads etc)', 1, '2013-04-26 10:27:00', 'Active'),
	(9, 'V', 'Cost estimation refused', 'customer has refused an OOW quote', 1, '2013-04-26 10:27:00', 'Active'),
	(10, 'Y', 'Return without repair', 'handset is returned back to customer with/without authorisation code', 1, '2013-04-26 10:27:00', 'Active'),
	(11, 'Y3', 'Product transport/forwarding', 'handset is forwarded onto a higher level repair centre', 1, '2013-04-26 10:27:00', 'Active'),
	(12, 'Z1', 'Product Exchange (repair too expensive)', 'handset replaced as repair costs exceeds replacement cost', 1, '2013-04-26 10:27:00', 'Active'),
	(13, 'Z2', 'Product Exchange (multiple prev repairs)', 'handset has had multiple previous repair attempts', 1, '2013-04-26 10:27:01', 'Active'),
	(14, 'Z3', 'Product Exchange (parts not available)', 'handset exchanged due to parts are DNA or SNA', 1, '2013-04-26 10:27:01', 'Active'),
	(15, 'Z6', 'Product Exchange (on request of OEM)', 'handset exchanged as requested by the manufacturer', 1, '2013-04-26 10:27:01', 'Active');

INSERT IGNORE INTO `part_repair_code_subset` (`PartRepairCodeID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 106, 11, 1, '2013-04-26 10:48:03'),
	(2, 106, 11, 1, '2013-04-26 10:48:03'),
	(3, 106, 11, 1, '2013-04-26 10:48:03'),
	(4, 106, 11, 1, '2013-04-26 10:48:03'),
	(5, 106, 11, 1, '2013-04-26 10:48:03'),
	(6, 106, 11, 1, '2013-04-26 10:48:03'),
	(7, 106, 11, 1, '2013-04-26 10:48:03'),
	(8, 106, 11, 1, '2013-04-26 10:48:03'),
	(9, 106, 11, 1, '2013-04-26 10:48:03'),
	(10, 106, 11, 1, '2013-04-26 10:48:03'),
	(11, 106, 11, 1, '2013-04-26 10:48:03'),
	(12, 106, 11, 1, '2013-04-26 10:48:03'),
	(13, 106, 11, 1, '2013-04-26 10:48:03'),
	(14, 106, 11, 1, '2013-04-26 10:48:03'),
	(15, 106, 11, 1, '2013-04-26 10:48:03');

INSERT IGNORE INTO `part_section_code` (`PartSectionCodeID`, `PartSectionCode`, `PartSectionCodeDescription`, `PartSectionCodeComment`, `ModifiedUserID`, `ModifiedDate`, `Status`) VALUES
	(20, 'BCH', 'Battery Charge', 'hanbdset will not charge the battery - charger and battery good', 1, '2013-04-26 10:16:23', 'Active'),
	(21, 'PSU', 'Power supply', 'charger at fault - handset and battery good', 1, '2013-04-26 10:16:23', 'Active'),
	(22, 'ANT', 'Antenna section', 'no/poor signal due to antenna fault', 1, '2013-04-26 10:16:23', 'Active'),
	(23, 'PWA', 'Power amp section', 'PA at fault - cannot call out or handset draws high current', 1, '2013-04-26 10:16:23', 'Active'),
	(25, 'RFU', 'RF unit', 'RF circuit/IC at fault', 1, '2013-04-26 10:16:23', 'Active'),
	(26, 'FLX', 'Flexible PCB', 'FPCB fault causing display failures - slider models', 1, '2013-04-26 10:16:24', 'Active'),
	(27, 'LCD', 'LCD screen', 'LCD fault', 1, '2013-04-26 10:16:24', 'Active'),
	(30, 'APD', 'Audio processing', 'audio fault due to audio IC/circuit', 1, '2013-04-26 10:16:24', 'Active'),
	(31, 'MIC', 'Microphone section', 'mic at fault', 1, '2013-04-26 10:16:25', 'Active'),
	(32, 'SPK', 'Speaker', 'speaker at fault', 1, '2013-04-26 10:16:25', 'Active'),
	(33, 'XXX', 'Cabinet/cosmetics parts', 'faults with any cosmetic parts', 1, '2013-04-26 10:16:25', 'Active'),
	(34, 'VPD', 'Video processing - digital', 'unable to capture pictures or record video - camera or circuit at fault', 1, '2013-04-26 10:16:25', 'Active'),
	(35, 'SET', 'Handset', 'all other hardware faults - bluetooth/IR/memory card reader etc', 1, '2013-04-26 10:16:25', 'Active'),
	(36, 'FMW', 'Firmware', 'any fault cured by firmware upgrade', 1, '2013-04-26 10:16:25', 'Active');

INSERT IGNORE INTO `part_section_code_subset` (`PartSectionCodeID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(20, 106, 11, 1, '2013-04-26 10:47:19'),
	(21, 106, 11, 1, '2013-04-26 10:47:19'),
	(22, 106, 11, 1, '2013-04-26 10:47:19'),
	(23, 106, 11, 1, '2013-04-26 10:47:19'),
	(25, 106, 11, 1, '2013-04-26 10:47:19'),
	(26, 106, 11, 1, '2013-04-26 10:47:19'),
	(27, 106, 11, 1, '2013-04-26 10:47:19'),
	(30, 106, 11, 1, '2013-04-26 10:47:19'),
	(31, 106, 11, 1, '2013-04-26 10:47:19'),
	(32, 106, 11, 1, '2013-04-26 10:47:19'),
	(33, 106, 11, 1, '2013-04-26 10:47:19'),
	(34, 106, 11, 1, '2013-04-26 10:47:19'),
	(35, 106, 11, 1, '2013-04-26 10:47:19'),
	(36, 106, 11, 1, '2013-04-26 10:47:19');

INSERT IGNORE INTO `symptom_code` (`SymptomCodeID`, `SymptomCode`, `SymptomCodeDescription`, `SymptomCodeComment`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '110', 'No power on', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(2, '11A', 'Powers up but no operation', 'handset freezes when/after powering up', 1, '2013-04-19 11:18:42'),
	(3, '11B', 'Cyclic power on/off', 'handset resets', 1, '2013-04-19 11:18:42'),
	(4, '11D', 'Set switches off by itself', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(5, '11J', 'Battery not recognised', 'customers/handset battery at fault and replaced', 1, '2013-04-19 11:18:42'),
	(6, '117', 'Short operating time/short battery life', '', 1, '2013-04-19 11:18:42'),
	(7, '120', 'Charging problem', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(8, '121', 'No charging battery', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(9, '28B', 'Faulty handsfree operation', 'PHF at fault', 1, '2013-04-19 11:18:42'),
	(10, '210', 'No reception', 'no signal strength/indicator', 1, '2013-04-19 11:18:42'),
	(11, '220', 'Poor reception', 'low signal strength/indicator', 1, '2013-04-19 11:18:42'),
	(12, '231', 'No transmission', 'cannot make outgoing calls', 1, '2013-04-19 11:18:42'),
	(13, '131', 'No display', 'blank/dead screen output', 1, '2013-04-19 11:18:42'),
	(14, '139', 'Display dim/too dark', 'contrast faults', 1, '2013-04-19 11:18:42'),
	(15, '13F', 'Missing segments', 'pixels/segments missing', 1, '2013-04-19 11:18:42'),
	(16, '13B', 'No backlight', 'no LCD LED operation', 1, '2013-04-19 11:18:42'),
	(17, '13G', 'Problem with ambient light', 'ambient light sensor at fault', 1, '2013-04-19 11:18:42'),
	(18, '382', 'Scratch on display', 'for refurb use only as per requirements by networks', 1, '2013-04-19 11:18:42'),
	(19, '741', 'Touchscreen not working', 'Touchscreen does not respond to any presses', 1, '2013-04-19 11:18:42'),
	(20, '742', 'Touch screen lock up', 'Touchscreen does work but locks up after a few presses', 1, '2013-04-19 11:18:42'),
	(21, '749', 'Touch screen surface damaged', 'Surface damage is preventing normal operation (also use for refurb)', 1, '2013-04-19 11:18:42'),
	(22, '17F', 'Faulty switch/button', 'intermittent and constant faults all keys', 1, '2013-04-19 11:18:42'),
	(23, '745', 'Inoperative keys', 'none of the keypad functions', 1, '2013-04-19 11:18:42'),
	(24, '132', 'Faulty LED operation', 'no keypad LED operation', 1, '2013-04-19 11:18:42'),
	(25, '277', 'No ringing tone', 'cannot hear handset ringtones', 1, '2013-04-19 11:18:42'),
	(26, '514', 'No audio playback', 'MP3 audio fault', 1, '2013-04-19 11:18:42'),
	(27, '516', 'No sound from speaker', 'loudspeaker faults', 1, '2013-04-19 11:18:42'),
	(28, '517', 'No sound from earphone/headphone', 'earpiece/headset faults', 1, '2013-04-19 11:18:42'),
	(29, '518', 'No microphone sound', 'no outgoing audio', 1, '2013-04-19 11:18:42'),
	(30, '532', 'Distorted audio', 'poor quality audio from earpiece, mic, loudspeaker or headsets', 1, '2013-04-19 11:18:42'),
	(31, '670', 'Mechanical operation problem', 'slider/flip mechanical operation faults', 1, '2013-04-19 11:18:42'),
	(32, '16E', 'External surface damage', 'paint peeling, plastic cracking - not caused by customer misuse', 1, '2013-04-19 11:18:42'),
	(33, '37D', 'Faulty picture capture function', 'cannot take pictures or make videos', 1, '2013-04-19 11:18:42'),
	(34, '788', 'No Bluetooth/WiFi/IR', 'can\'t find for other Bluetooth/WiFi/IR Device', 1, '2013-04-19 11:18:42'),
	(35, '785', 'Incompatible with other system', 'Bluetooth not working with handsfree/other devices', 1, '2013-04-19 11:18:42'),
	(36, '21B', 'No infrared reception/emission', 'intermittent and constant faults', 1, '2013-04-19 11:18:42'),
	(37, '212', 'No FM Reception', 'FM radio feature does not work', 1, '2013-04-19 11:18:42'),
	(38, '160', 'Physical damage', 'Damaged caused by customer misuse - only for BER claims', 1, '2013-04-19 11:18:42'),
	(39, '171', 'Faulty clock function', 'issues with clock or alarm', 1, '2013-04-19 11:18:42'),
	(40, '181', 'Test and Check', 'NFF when no customer symptom is reported', 1, '2013-04-19 11:18:42'),
	(41, '18C', 'Software upgrade requested', 'Customer/network has requested latest approved SW to be loaded', 1, '2013-04-19 11:18:42'),
	(42, '717', 'No data storage', 'cannot read write to memory card or HDD (i.e. SGH-i300)', 1, '2013-04-19 11:18:42'),
	(43, '718', 'No data communication', 'WAP/GPRS/3G call faults', 1, '2013-04-19 11:18:42'),
	(44, '76C', 'Insert SIM card', 'handset does not recognise SIM card inserted', 1, '2013-04-19 11:18:42');

INSERT IGNORE INTO `symptom_code_subset` (`SymptomCodeID`, `SymptomTypeID`, `SymptomConditionID`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 1, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(2, 1, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(3, 1, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(4, 1, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(5, 2, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(6, 2, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(7, 2, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(8, 2, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(9, 2, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(10, 3, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(11, 3, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(12, 3, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(13, 4, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(14, 4, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(15, 4, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(16, 4, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(17, 4, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(18, 4, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(19, 5, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(20, 5, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(21, 5, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(22, 6, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(23, 6, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(24, 6, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(25, 7, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(26, 7, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(27, 7, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(28, 7, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(29, 7, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(30, 7, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(31, 8, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(32, 8, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(33, 9, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(34, 10, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(35, 10, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(36, 11, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(37, 12, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(38, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(39, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(40, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(41, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(42, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(43, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10'),
	(44, 13, NULL, 106, 11, 1, '2013-04-26 10:50:10');


INSERT IGNORE INTO `symptom_type` (`SymptomTypeID`, `SymptomTypeName`, `ManufacturerID`, `RepairSkillID`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, 'Power', 106, 2, 1, '2013-04-19 11:20:06'),
	(2, 'Battery/Charging/Accessories', 106, 2, 1, '2013-04-19 11:20:06'),
	(3, 'TX/RX/Signal', 106, 2, 1, '2013-04-19 11:20:06'),
	(4, 'Display', 106, 2, 1, '2013-04-19 11:20:06'),
	(5, 'Touch Screen', 106, 2, 1, '2013-04-19 11:20:06'),
	(6, 'Keypad', 106, 2, 1, '2013-04-19 11:20:06'),
	(7, 'Audio', 106, 2, 1, '2013-04-19 11:20:06'),
	(8, 'Cosmetic', 106, 2, 1, '2013-04-19 11:20:06'),
	(9, 'Camera', 106, 2, 1, '2013-04-19 11:20:06'),
	(10, 'Bluetooth', 106, 2, 1, '2013-04-19 11:20:06'),
	(11, 'Infra Red', 106, 2, 1, '2013-04-19 11:20:06'),
	(12, 'FM Radio', 106, 2, 1, '2013-04-19 11:20:06'),
	(13, 'Software/other hardware', 106, 2, 1, '2013-04-19 11:20:06');

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.242');
