# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.227');


# ---------------------------------------------------------------------- #
# Add Table currency                                                     #
# ---------------------------------------------------------------------- #
CREATE TABLE currency ( 
						CurrencyID INT(10) NOT NULL AUTO_INCREMENT, 
						CurrencyName VARCHAR(50) NULL DEFAULT NULL, 
						CurrencyCode VARCHAR(5) NULL DEFAULT NULL, 
						CurrencySymbol VARCHAR(5) NULL DEFAULT NULL, 
						AutomaticExchangeRateCalculation ENUM('Yes','No') NOT NULL DEFAULT 'Yes', 
						ConversionCalculation ENUM('Multiply','Divide') NOT NULL DEFAULT 'Multiply', 
						ExchangeRate DECIMAL(10,4) NULL DEFAULT NULL, 
						ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
						ExchangeRateLastModified DATETIME NOT NULL, 
						ModifiedUserID INT NOT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', 
						PRIMARY KEY (CurrencyID) 
					  ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Add Table currency_history                                             #
# ---------------------------------------------------------------------- #
CREATE TABLE currency_history (
								CurrencyHistoryID INT(10) NOT NULL AUTO_INCREMENT, 
								CurrencyID INT NOT NULL, 
								ChangeTimestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
								RateFrom DECIMAL(10,4) NULL, RateTo DECIMAL(10,4) NULL, 
								ChangedBy VARCHAR(50) NULL, 
								PRIMARY KEY (CurrencyHistoryID) 
							  ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Add and Populate Table currency_rate                                   #
# ---------------------------------------------------------------------- #
CREATE TABLE `currency_rate` (
								`CurrencyRateID` int(11) NOT NULL AUTO_INCREMENT,
								`Currency` char(3) NOT NULL COMMENT 'ISO Currency Code',
								`Rate` decimal(10,5) NOT NULL COMMENT 'Value of the currency equivalent to 1 EUR',
								`ModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
								PRIMARY KEY (`CurrencyRateID`),
								UNIQUE KEY `CURRENCY_CODE` (`Currency`)
							  ) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (1,'USD','1.30520','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (2,'JPY','129.54000','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (3,'BGN','1.95580','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (4,'CZK','25.87100','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (5,'DKK','7.45520','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (6,'GBP','0.84970','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (7,'HUF','294.55000','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (8,'LTL','3.45280','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (9,'LVL','0.70060','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (10,'PLN','4.10400','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (11,'RON','4.38900','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (12,'SEK','8.33730','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (13,'CHF','1.21670','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (14,'NOK','7.48200','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (15,'HRK','7.61250','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (16,'RUB','40.49500','2013-04-15 11:02:14');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (17,'TRY','2.33530','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (18,'AUD','1.23960','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (19,'BRL','2.58270','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (20,'CAD','1.32070','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (21,'CNY','8.08190','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (22,'HKD','10.13030','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (23,'IDR','12670.52000','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (24,'ILS','4.73680','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (25,'INR','71.16600','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (26,'KRW','1474.85000','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (27,'MXN','15.74400','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (28,'MYR','3.96620','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (29,'NZD','1.51820','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (30,'PHP','53.90300','2013-04-15 11:02:15');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (31,'SGD','1.61530','2013-04-15 11:02:16');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (32,'THB','37.89000','2013-04-15 11:02:16');
insert  into `currency_rate`(`CurrencyRateID`,`Currency`,`Rate`,`ModifiedDate`) values (33,'ZAR','11.65650','2013-04-15 11:02:16');

# ---------------------------------------------------------------------- #
# Add Table questionnaire                                                #
# ---------------------------------------------------------------------- #
CREATE TABLE questionnaire (
							QuestionnaireID int(11) NOT NULL AUTO_INCREMENT, 
							JobID int(11) NOT NULL, 
							CustomerID int(11) NOT NULL, 
							QuestionnaireType char(5) NOT NULL, 
							ResponseDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
							PRIMARY KEY (QuestionnaireID), 
							KEY IDX_Job (JobID), 
							KEY IDX_Customer (CustomerID), 
							KEY IDX_QuestionnaireType (QuestionnaireType), 
							KEY IDX_ResponseDate (ResponseDate)
						   );

# ---------------------------------------------------------------------- #
# Add Table questionnaire_data                                           #
# ---------------------------------------------------------------------- #
CREATE TABLE questionnaire_data (
								 QuestionnaireDataID int(11) NOT NULL AUTO_INCREMENT, 
								 QuestionnaireID int(11) NOT NULL, 
								 AttributeID int(11) NOT NULL, 
								 StringValue varchar(60) DEFAULT NULL, 
								 NumericValue decimal(10,2) DEFAULT NULL, 
								 TextValue varchar(2000) DEFAULT NULL, 
								 PRIMARY KEY (QuestionnaireDataID), 
								 KEY IDX_Questionnaire (QuestionnaireID), 
								 KEY IDX_Attribute (AttributeID), 
								 KEY IDX_StringValue (StringValue), 
								 KEY IDX_NumericValue (NumericValue) 
								);

# ---------------------------------------------------------------------- #
# Add Table questionnaire_attribute                                      #
# ---------------------------------------------------------------------- #
CREATE TABLE questionnaire_attribute ( 
										QuestionnaireAttributeID int(11) NOT NULL AUTO_INCREMENT, 
										AttributeName varchar(40) NOT NULL, 
										PRIMARY KEY (QuestionnaireAttributeID), 
										KEY IDX_Attribute (AttributeName) 
									  );

# ---------------------------------------------------------------------- #
# Add Table questionnaire_log                                            #
# ---------------------------------------------------------------------- #
CREATE TABLE questionnaire_log (
								QuestionnaireLog char(36) NOT NULL, 
								JobID int(11) NOT NULL, 
								Brand varchar(40) NOT NULL, 
								Type char(5) NOT NULL, 
								Created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 
								Response enum('No','Yes') NOT NULL DEFAULT 'No', 
								PRIMARY KEY (QuestionnaireLog) 
								);
								
# ---------------------------------------------------------------------- #
# Add Table user_reports                                                 #
# ---------------------------------------------------------------------- #
CREATE TABLE user_reports (
							UserReportID INT(11) NOT NULL AUTO_INCREMENT, 
							UserID INT(11) NOT NULL, 
							ReportName VARCHAR(250) NOT NULL, 
							ReportDescription VARCHAR(250) NULL DEFAULT NULL, 
							ReportStructure TEXT NOT NULL, 
							PRIMARY KEY (UserReportID), 
							CONSTRAINT user_TO_user_reports FOREIGN KEY (UserID) REFERENCES user (UserID) ON UPDATE NO ACTION ON DELETE NO ACTION 
						   ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.228');