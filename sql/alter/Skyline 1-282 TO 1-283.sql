# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.282');

# ---------------------------------------------------------------------- #
# Andris Stock Changes   												 #
# ---------------------------------------------------------------------- # 
CREATE TABLE sp_part_status_colour ( SpPartStatusColourID INT(10) NOT NULL AUTO_INCREMENT, PartStatusID INT(10) NULL DEFAULT NULL, Colour VARCHAR(8) NULL DEFAULT NULL, TextColour ENUM('Black','White') NOT NULL DEFAULT 'Black', Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', ServiceProviderID INT(10) UNSIGNED NOT NULL DEFAULT '0', CreatedUserID INT(11) NULL DEFAULT NULL, PRIMARY KEY (SpPartStatusColourID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE service_provider_model_to_sp_part_stock_template ( ServiceProviderModelID INT(11) NULL DEFAULT NULL, SpPartStockTemplateID INT(11) NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', CreatedUserID INT(11) NULL DEFAULT NULL ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.283');
