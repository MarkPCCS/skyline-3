# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.193');

# ---------------------------------------------------------------------- #
# Add table "service_provider"                                                #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` CHANGE COLUMN `DiaryAllocationOnly` `DiaryType` ENUM('FullViamente','NoViamente','AllocationOnly') NOT NULL DEFAULT 'FullViamente' AFTER `Multimaps`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.194');
