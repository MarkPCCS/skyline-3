<script type="text/javascript" >
  
$(document).ready(function() {

     $('#pono_wildCard').click(function(event) {
     
        if(!$('#policySearch').val())
        {
            $('#policySearch').focus();
            $("[for='policySearch']").css("display", "none");
        }
        
        insertAtCursor(document.policySearchForm.policySearch, "*");
        
        
     });
     $("#pono_wildCardForm").hide();
     $("#pono_wildCardHelp").colorbox(

                    { 
                            inline:true, 
                            href:"#pono_wildCardForm", 
                            title: 'Wild Card Search',
                            opacity: 0.75,
                            height:290,
                            width:700,
                            overlayClose: false,
                            escKey: false,
                            onOpen:function(){
                                $("#pono_wildCardForm").show("fast");
                            },                            
                            onCleanup:function(){
                                $("#pono_wildCardForm").hide("fast");
                            }

                    }
                );
                
     
     
     
                


});
  
    
</script>    


<form id="pono_wildCardForm" name="pono_wildCardForm">

    <fieldset>
         <legend >Wild Card Search</legend>
         
         <p>
             <br>You can create a wild card search by inserting * before or after the string you are searching for.
         </p>
         <p>
             Example1: Berry* would deliver all occurrences of Berry, Berryman, etc.
         </p>
          <p>
              Example2: *erry* would deliver all occurrences of Berry, Berryman, Merry, Merryman etc.
         </p>
         
    </fieldset>
    
</form>      

<form id="policySearchForm" name="policySearchForm" method="get"
            action="{$_subdomain}/index/policySearch" >
        
        <fieldset style="padding-right: 40px;padding-top: 18px;padding-bottom:0px">
            
            {* <legend title="{$ref}">Product Search</legend> *}
            <legend  >Policy Search</legend>

            <p>
                <input type="text" name="policySearch" id="policySearch" 
                       value="{$policySearch}" class="text bigTextBox"  
                       title="Please enter a Policy Number, Surname or Post Code"/>
                <span style="float:right;">
                    <a  href="#"  onclick='alert("Policy Search API not found, therefore, the required Policy information cannot be retrieved. Please contact your System Supervisor.");' >Search</a>
                </span>
            </p>
            
            <p style="margin-bottom: 0;" >
                
                
                
		{if isset($superadmin)}
		    {if $superadmin}
                        <div class="clearfix">
			<a style="float:left;display:inline;" tabIndex="16" href="#" id="pono_wildCard"   >Wild Card Search</a>
			<img src="{$_subdomain}/css/Skins/{$_theme}/images/help.png" alt="Click here to see the help." title="Click here to see the help." width="20" height="20" id="pono_wildCardHelp" style="float:left;padding-left:5px;padding-top:4px;" >
			<span class="noResults" >
			     
			</span>
                        </div>
		    {/if}
		{/if}
		
               
            </p>
        
        </fieldset>
        
 </form>

  
            
  
