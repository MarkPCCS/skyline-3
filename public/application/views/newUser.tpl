{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $LoginPage}
{/block}

{block name=scripts}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

<script type="text/javascript">

$(document).ready(function(){

    $('input[type=text],input[type=password]').jLabel().attr('autocomplete','off').blur(function() {  $('#error,#error2').hide('slow'); } );
    $('#tempass').focus();
   
    /* =======================================================
     *
     * set tab on return for input elements with form submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } );
    
});

function validateForm() {
    if ($('#tempass').val()=='') {
        $('#error').html("{$page['Errors']['temp']}").show('slow');
        $('#tempass').focus();
        return false;
    }
    $('#function').val('login');
    return true;
}

function validateForm2() {
    if ($('#branch').val()=='') {        
        $('#error2').html("{$page['Errors']['branch']}").show('slow');
        $('#branch').focus();
        return false;
    }
    if ($('#name').val()=='') {
        $('#error2').html("{$page['Errors']['name']}").show('slow');
        $('#name').focus();
        return false;
    }
    if ($('#contact').val()=='') {
        $('#error2').html("{$page['Errors']['contact']}").show('slow');
        $('#contact').focus();
        return false;
    }
    $('#function').val('problem');
    return true;
}
</script>

{/block}

{block name=body}
<div class="main" id="newuser">
        
        <form id="newUserForm" name="newUserForm" method="post" action="{$_subdomain}/Login/newUser" class="prepend-5 span-14 last">
            
           <fieldset>
               
               <input id="function" name="function" type="hidden" value="login" />
               
                <legend title="IM5006">{$page['Text']['legend']|escape:'html'}</legend>
                {* <div class="serviceInstructionsBar">IM5006</div> *}
                
                <p class="information">
                    {$page['Text']['instructions']|escape:'html'}
                </p>
                
                <p class="information">
                    {$page['Text']['temp_password']|escape:'html'}
                </p>
                
                <p style="text-align: center; margin: 0; ">
                    <label for="tempass">{$page['Labels']['tempass']|escape:'html'}</label>
                    <input type="password" name="tempass" id="tempass" value="" class="text auto-submit" tabIndex="1" />
                </p>
                
                {if $error eq ''}
                <p id="error" class="formError" style="display: none; text-align: center;" />
                {else}
                <p id="error" class="formError" style="text-align: center;" >{$error}</p>
                {/if}
                
                <p style="text-align: center; margin: 0; ">
                    <span style="display: inline-block; width: 312px; text-align: center; ">
                        <a href="javascript:if (validateForm()) document.newUserForm.submit();" tabIndex="2">{$page['Buttons']['submit']|escape:'html'}</a>
                    </span>
                </p>
                  
                <p class="information" style="text-align: center; margin-top: 0;">
                    {$page['Text']['not_issued']|escape:'html'}
                </p>
                
                <p class="information" style="text-align: center; margin-bottom: 10px;">
                    {$page['Text']['issue']|escape:'html'}
                </p>
                
                <p style="text-align: center;">
                    <select name="branch" id="branch" class="text" tabIndex="3" >
                        <option value="" >{$page['Text']['select_branch']|escape:'html'}</option>
                        {foreach $branches as $b}
                        <option value="{$b.BranchID}" {if $b.BranchID eq $branch}selected{/if}>{$b.BranchName|escape:'html'}</option>
                        {/foreach}
                    </select>
                </p>                
                               
                <p style="text-align: center;">
                    <label for="name">{$page['Labels']['name']|escape:'html'}</label>
                    <input type="text" name="name" id="name" value="" class="text" tabIndex="4" />
                </p>
                
                <p style="text-align: center;">
                    <label for="contact">{$page['Labels']['contact']|escape:'html'}</label>
                    <input type="text" name="contact" id="contact" value="" class="text" tabIndex="5" />
                </p>
                
                <p id="error2" class="formError" style="text-align: center;display: none;"></p>

                <p style="text-align: center; margin: 0;">
                    <span style="display: inline-block; width: 312px; text-align: center;">
                        <a href="javascript:if (validateForm2()) document.newUserForm.submit();" tabIndex="6">{$page['Buttons']['report']|escape:'html'}</a>
                    </span>
                </p>
                
                <p style="text-align: right;">
                    <a href="{$_subdomain}/index/index" tabIndex="7">{$page['Buttons']['cancel']|escape:'html'}</a>
                </p>
                
            </fieldset>
  
        </form>
    
</div>
{/block}