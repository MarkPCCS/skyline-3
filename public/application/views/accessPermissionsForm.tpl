 <script type="text/javascript">   
     
 $(document).ready(function() {
 
     $('#Colour').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '150px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
    
    $(".Color").css({ width: "25px", height: "24px", padding: "0px" });
  
  
   }); 
   
  </script>  
    
    <div id="APFormPanel" class="SystemAdminFormPanel" >
    
                <form id="APForm" name="APForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                       
                         
                         
                         <p>
                            <label class="fieldLabel" for="Name" >{$page['Labels']['access_permission']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text" maxlength="40"  name="Name" value="{$datarow.Name|escape:'html'}" id="Name" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="Description" >{$page['Labels']['description']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; <textarea class="text" style="width:312px;height:150px;" name="Description" id="Description"  >{$datarow.Description|escape:'html'}</textarea>

                          </p>


                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    



                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="PermissionID"  value="{$datarow.PermissionID|escape:'html'}" >
                                   
                                        <input type="submit" name="update_save_btn" class="textSubmitButton" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                        &nbsp;&nbsp;
                                        
                                        <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
          
