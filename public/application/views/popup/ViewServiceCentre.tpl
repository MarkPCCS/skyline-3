<form action="#" id="ViewServiceCentre" name="ViewServiceCentre" method="post">
    <fieldset>
        <legend title="" style="text-transform: capitalize;">{$CompanyName|lower|escape:'html'} - Company Information</legend>
        <p>
            <span class="span-3" style="padding-left: 0px;"><strong>Opening Hours:</strong></span>
            <span class="span-6">Monday to Friday: {$WeekdaysOpenTime|date_format:'%H:%M'} to {$WeekdaysCloseTime|date_format:'%H:%M'}</span>
            <span class="span-6 last">
                {if $SaturdayOpenTime ne null and $SaturdayOpenTime ne 0 and $SaturdayCloseTime ne null and $SaturdayCloseTime ne 0}
                Saturday: {$SaturdayOpenTime|date_format:'%H:%M'} to {$SaturdayCloseTime|date_format:'%H:%M'}
                {/if}
            </span>
        </p>

        {if $ContactPhone or $ContactPhoneExt or $ContactEmail}
            
        <p>
                {if $ContactPhone}
                <span class="span-4"> (T) {$ContactPhone} {if $ContactPhoneExt} Ex. {$ContactPhoneExt} {/if}</span>
                {/if}
                
                {if $ContactEmail}
                <span class="span-4"> (E) {$ContactEmail} </span>  
                {/if}
                
                {if $PublicWebsiteAddress}
                <span class="span-7"> (W) <a href="{$PublicWebsiteAddress}" target="_blank" >{$PublicWebsiteAddress}</a></span>  
                {/if}
                
        </p>
        
        {/if}
        
        <p><strong>Principle Contacts:</strong><br >
                {if $AdminSupervisorName && $AdminSupervisorEmail}
                <span style="margin:4px 15px 4px 4px;" >&bullet; &nbsp;&nbsp;Admin Supervisor: {if $AdminSupervisorName|trim neq ''} {$AdminSupervisorName|escape:'html'} {else} -- {/if}</span> {if $AdminSupervisorEmail}<span>(E) {$AdminSupervisorEmail|escape:'html'}</span>{/if}<br>
                {/if}
                
                {if $ServiceManagerName && $ServiceManagerEmail}
                <span style="margin:4px 15px 4px 4px;" >&bullet; &nbsp;&nbsp;Workshop Supervisor: {if $ServiceManagerName|trim neq ''} {$ServiceManagerName|escape:'html'} {else} -- {/if}</span> {if $ServiceManagerEmail}<span>(E) {$ServiceManagerEmail|escape:'html'}</span>{/if}<br>
                {/if}
                
                {if $GeneralManagerName && $GeneralManagerEmail}
                <span style="margin:4px 15px 4px 4px;" >&bullet; &nbsp;&nbsp;General Manager: {if $GeneralManagerName|trim neq ''} {$GeneralManagerName|escape:'html'} {else} -- {/if}</span> {if $GeneralManagerEmail}<span>(E) {$GeneralManagerEmail|escape:'html'}</span>{/if}<br>
                {/if}
        </p>
        <p><strong>Service Provided:</strong><br />
            {$ServiceProvided|escape:'html'}
        </p>
        <p><strong>Synopsis of Business:</strong><br />
            {$SynopsisOfBusiness|escape:'html'}
        </p>
        
    </fieldset>
</form>