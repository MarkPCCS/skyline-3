<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Postcode Allocations Page in Job Allocation section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class PostcodeAllocations extends CustomModel {
    
    private $conn;
    
    private $table                                       = "postcode_allocation";
    private $table_network                               = "network";
    private $table_network_service_provider              = "network_service_provider";
    
    
    
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->table 
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        $NetworkID       = isset($args['firstArg'])?$args['firstArg']:'';
        $ManufacturerID  = isset($args['secondArg'])?$args['secondArg']:'';
        
        
        
        $dbTablesColumns  = array('T1.PostcodeAllocationID', 'T2.CompanyName', 'T3.RepairSkillName',  'T4.ClientName', 'T5.Type', 'T6.ServiceTypeName',  'T1.OutOfArea', 'COUNT(T7.PostcodeAllocationID) AS AreasCovered');
        $dbTables         = $this->table." AS T1 
                            LEFT JOIN service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID
                            LEFT JOIN repair_skill AS T3 ON T1.RepairSkillID=T3.RepairSkillID
                            LEFT JOIN client  AS T4 ON T1.ClientID =T4.ClientID 
                            LEFT JOIN job_type  AS T5 ON T1.JobTypeID=T5.JobTypeID
                            LEFT JOIN service_type AS T6 ON T1.ServiceTypeID=T6.ServiceTypeID
                            LEFT JOIN postcode_allocation_data AS T7 ON T1.PostcodeAllocationID=T7.PostcodeAllocationID

                            ";
                

        if($NetworkID!='')
        {
            if($ManufacturerID)
            {
                $args['where']    = "T1.NetworkID='".$NetworkID."' AND T1.ManufacturerID='".$ManufacturerID."' GROUP BY T1.PostcodeAllocationID";
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
            }   
            else
            {    
                $args['where']    = "T1.NetworkID='".$NetworkID."' GROUP BY T1.PostcodeAllocationID";
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
            }
        }
        else
        {
            $args['where']    = "T1.PostcodeAllocationID='0' GROUP BY T1.PostcodeAllocationID";
            
             $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
        }
       
        //$this->controller->log(var_export($output, true));
       
        return  $output;
        
    }
    
    
    /*
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    public function processData($args) {
         
	if(!isset($args['PostcodeAllocationID']) || !$args['PostcodeAllocationID']) {
	    return $this->create($args);
	} else {
	    return $this->update($args);
	}
	
     }
    
     
     
     /**
     * Description
     * 
     * This method gets ServiceProviderID for given account number.
     * 
     * @param int $AccountNo.
     * @param int $NetworkID. 
   
     * @global $this->table_network_service_provider
    
     * @return int Service Provider ID on success or zero on fail.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getServiceProviderID($AccountNo, $NetworkID) {
         
            $prepArgs = array(
            
           ':NetworkID' => $NetworkID, 
           ':AccountNo' => $AccountNo, 
           ':Status' => "Active" 
          
           ); 
         
          /* Execute a prepared statement by passing an array of values */
            $sql = 'SELECT ServiceProviderID FROM '.$this->table_network_service_provider.' WHERE AccountNo=:AccountNo AND NetworkID=:NetworkID AND Status=:Status';

            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute($prepArgs);
            $result = $fetchQuery->fetch();


            if(is_array($result) && $result['ServiceProviderID'])
            {
                    return $result['ServiceProviderID'];
            }

            return 0;
     }
     
     
    
    /*
     * Description
     * 
     * This method is used for to check for duplicate row.
     * 
     * @param int $PostcodeAllocationID 
     * @param array $args It contains list of fields
     
     * @global $this->table
     * 
     * @return int It returns primary key if it was found other wise it returns zero.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function isExists($PostcodeAllocationID, $args) {
        
	$prepArgs = array(
           ':NetworkID' => $args['NetworkID'], 
           ':ManufacturerID' => $args['ManufacturerID'], 
           ':RepairSkillID' => $args['RepairSkillID'], 
           ':JobTypeID' => $args['JobTypeID'], 
           ':ServiceTypeID' => $args['ServiceTypeID'], 
           ':ClientID' => $args['ClientID'], 
           ':ServiceProviderID' => $args['ServiceProviderID'], 
           ':OutOfArea' => $args['OutOfArea'],  
           ':PostcodeAllocationID' => $PostcodeAllocationID
        ); 
	
	//$this->controller->log("PREP ARGS: " . var_export($prepArgs,true));
         
	if($prepArgs[":JobTypeID"]==null) {
	    unset($prepArgs[":JobTypeID"]);
	}
	if($prepArgs[":ServiceTypeID"]==null) {
	    unset($prepArgs[":ServiceTypeID"]);
	}
	if($prepArgs[":ClientID"]==null) {
	    unset($prepArgs[":ClientID"]);
	}
	
	//$this->controller->log("PREP ARGS: " . var_export($prepArgs,true));
	
        //Execute a prepared statement by passing an array of values
	
        $sql = 'SELECT	    PostcodeAllocationID 
		FROM	    ' . $this->table . ' 
		WHERE	    NetworkID=:NetworkID AND 
			    ManufacturerID=:ManufacturerID AND
			    RepairSkillID=:RepairSkillID AND ' .
			    (($args["JobTypeID"]!=null) ? 'JobTypeID=:JobTypeID' : 'JobTypeID IS NULL') . ' AND ' .
			    (($args["ServiceTypeID"]!=null) ? 'ServiceTypeID=:ServiceTypeID' : 'ServiceTypeID IS NULL') . ' AND ' .
			    (($args["ClientID"]!=null) ? 'ClientID=:ClientID' : 'ClientID IS NULL') . ' AND
			    ServiceProviderID=:ServiceProviderID AND
			    OutOfArea=:OutOfArea AND
			    PostcodeAllocationID!=:PostcodeAllocationID';
	
	/*
        $sql = 'SELECT	    PostcodeAllocationID 
		FROM	    ' . $this->table . ' 
		WHERE	    NetworkID=:NetworkID AND 
			    ManufacturerID=:ManufacturerID AND
			    RepairSkillID=:RepairSkillID AND 
			    JobTypeID=:JobTypeID AND 
			    ServiceTypeID=:ServiceTypeID AND 
			    ClientID=:ClientID AND
			    ServiceProviderID=:ServiceProviderID AND
			    OutOfArea=:OutOfArea AND
			    PostcodeAllocationID!=:PostcodeAllocationID';
	*/
	
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute($prepArgs);
        $result = $fetchQuery->fetch();
        
	//$this->controller->log("RESULT: " . var_export($result,true));
	
        if(is_array($result) && $result['PostcodeAllocationID']) {
	    return $result['PostcodeAllocationID'];
        }
        
        return 0;
    
    }
    
    
   
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        
        if(isset($args['OutOfArea']) && $args['OutOfArea']=='1')
        {
            $args['OutOfArea'] = "Yes";
        }
        else
        {
            $args['OutOfArea'] = "No";
        }
        
        $finalFilesList = $noServiceProviders = array();
	
        //Preparing files list to be read and getting list of account numbers which are not assigned to the service proviers.
        if(isset($args['fileUploadPrefix']))
        {
            $targetDir     = APPLICATION_PATH."/uploads/postcodeAreas";
            $allFilesList  = scandir($targetDir);
            $filesList     = array();
            
            foreach($allFilesList AS $afKey=>$afValue)
            {
                if(strpos($afValue, $args['fileUploadPrefix']."-")===0)
                {
                    $fileNameArray       = explode("-", $afValue);
                    $AccountNo           = isset($fileNameArray[1]) ? $fileNameArray[1] : 0;
		    $ServiceProviderID   = $this->getServiceProviderID($AccountNo, $args['NetworkID']);
                    $createdDate =  date("Y-m-d H:i:s", filemtime($targetDir."/".$afValue));
                    $filesList[]         = array($afValue, $AccountNo, $ServiceProviderID, $createdDate);
                }         
            }
            
            uasort($filesList, function($a, $b) { return strnatcmp($a[3], $b[3]); });

            foreach($filesList AS $flKey=>$flValue)
            {
                if(!$flValue[2] && $flValue[1])
                {
                    $noServiceProviders[$flValue[1]] = $flValue[1];
                }
                else if($flValue[1] && $flValue[2])
                {
                    $finalFilesList[$flValue[1]] = $flValue;
                }
            }
            
        }
        
        $insert_Flag = 0;
	
	//$this->controller->log("FILES: " . var_export($finalFilesList,true));
	
        foreach($finalFilesList AS $fflKey=>$fflValue)
        {    
            $args['ServiceProviderID'] = $fflValue[2];
            $PostcodeAllocationID      = $this->isExists(0, $args);
            
	    //$this->controller->log("ARGS: " . var_export($args,true));
	    
	    //$this->controller->log("Postcode Allocation ID: " . $PostcodeAllocationID);
	    
	    if($PostcodeAllocationID) {
		
		$this->deletePostCodes($PostcodeAllocationID);
		
	    } else {  
		
		$result = false;

		/* Execute a prepared statement by passing an array of values */
		$sql = 'INSERT INTO '.$this->table.' (NetworkID, ManufacturerID, RepairSkillID, JobTypeID, ServiceTypeID, ClientID, ServiceProviderID, OutOfArea, CreatedDate, ModifiedUserID, ModifiedDate)
		VALUES(:NetworkID, :ManufacturerID, :RepairSkillID, :JobTypeID, :ServiceTypeID, :ClientID, :ServiceProviderID, :OutOfArea, :CreatedDate, :ModifiedUserID, :ModifiedDate)';

		$insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

		$values = array(
		    ':NetworkID' => $args['NetworkID'],
		    ':ManufacturerID' => $args['ManufacturerID'],
		    ':RepairSkillID' => $args['RepairSkillID'],
		    ':JobTypeID' => (isset($args['JobTypeID']) && $args['JobTypeID']!=0) ? $args['JobTypeID'] : null,
		    ':ServiceTypeID' => (isset($args['ServiceTypeID']) && $args['ServiceTypeID']!=0) ? $args['ServiceTypeID'] : null,
		    ':ClientID' => (isset($args['ClientID']) && $args['ClientID']!=0) ? $args['ClientID'] : null,
		    ':ServiceProviderID' => $args['ServiceProviderID'],
		    ':OutOfArea' => $args['OutOfArea'],
		    ':CreatedDate' => date("Y-m-d H:i:s"),
		    ':ModifiedUserID' => $this->controller->user->UserID,
		    ':ModifiedDate' => date("Y-m-d H:i:s")
		);

		$result =  $insertQuery->execute($values);

		$PostcodeAllocationID = $this->conn->lastInsertId();

            }
            
            if($PostcodeAllocationID) {
                $this->insertPostCodes($PostcodeAllocationID, $fflValue[0]);
                $insert_Flag = 1;
            }
        } 
        
	//$this->controller->log("INSERT FLAG: " . $insert_Flag);
	
        $noServiceProvidersError1 = "";
        $noServiceProvidersError2 = "";
        if($noServiceProviders)
        {
            $noServiceProvidersError1 = "&nbsp;&nbsp;".$this->controller->page['Errors']['no_service_provider_on_success'].": ".implode(",",$noServiceProviders);
            $noServiceProvidersError2 = "&nbsp;&nbsp;".$this->controller->page['Errors']['no_service_provider_on_fail'].": ".implode(",",$noServiceProviders);
        }
        
        //$this->controller->log(var_export($noServiceProviders, true));
        
        if($insert_Flag)
        {
                return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_inserted_msg'].$noServiceProvidersError1);
        }
        else
        {
            return array('status' => 'ERROR',
                        'message' => $this->controller->page['Errors']['data_not_processed'].$noServiceProvidersError2);
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @global $this->table_network
     *  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
         
         
         
       
        $dbTables         = $this->table." AS T1 
                            LEFT JOIN service_provider AS T2 ON T1.ServiceProviderID=T2.ServiceProviderID
                            LEFT JOIN repair_skill AS T3 ON T1.RepairSkillID=T3.RepairSkillID
                            LEFT JOIN client  AS T4 ON T1.ClientID =T4.ClientID 
                            LEFT JOIN job_type  AS T5 ON T1.JobTypeID=T5.JobTypeID
                            LEFT JOIN service_type AS T6 ON T1.ServiceTypeID=T6.ServiceTypeID
                            LEFT JOIN postcode_allocation_data AS T7 ON T1.PostcodeAllocationID=T7.PostcodeAllocationID
                            LEFT JOIN manufacturer AS T8 ON T1.ManufacturerID =T8.ManufacturerID
                            LEFT JOIN network AS T9 ON T1.NetworkID =T9.NetworkID
                            ";
                
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T1.PostcodeAllocationID, T2.CompanyName, T3.RepairSkillName, T4.ClientName, T5.Type, T6.ServiceTypeName, T1.OutOfArea, COUNT(T7.PostcodeAllocationID) AS AreasCovered, T8.ManufacturerName, T9.CompanyName AS NetworkName FROM '.$dbTables.' WHERE T1.PostcodeAllocationID=:PostcodeAllocationID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':PostcodeAllocationID' => $args['PostcodeAllocationID']));
        $result = $fetchQuery->fetch();
        
        if(is_array($result))
        {
            $result['PostCodeAreas'] = '';
            
            $pc_sql = 'SELECT PostcodeArea FROM postcode_allocation_data WHERE PostcodeAllocationID=:PostcodeAllocationID ORDER BY PostcodeArea';
            $pcFetchQuery = $this->conn->prepare($pc_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $pcFetchQuery->execute(array(':PostcodeAllocationID' => $args['PostcodeAllocationID']));
            $pc_result = $pcFetchQuery->fetchAll(PDO::FETCH_COLUMN, 0);
            if(is_array($pc_result))
            {
                $result['PostCodeAreas'] = implode(", ", $pc_result);
            }
        
        }    
        
        
        
        return $result;
     }
    
     
       /**
     * Description
     * 
     * This method is user for to convert csv file dat into array.
    
     * @param string $CSVFileName
     * 
     * @return array $postCodesResult 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
     public function readCSVFile($CSVFileName) {
         
         $postCodesResult = array();
         $row = 0;
        if (($handle = fopen(APPLICATION_PATH."/uploads/postcodeAreas/".$CSVFileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                
                if(isset( $data[0]) && $data[0])
                {
                    $postCodesResult[] = $data[0];
                }
               
            }
            fclose($handle);
        }
        return $postCodesResult; 
     }
     
     
      /**
     * Description
     * 
     * This method is used for to insert post codes for given PostcodeAllocationID.
     * @param int $PostcodeAllocationID
     * @param string $CSVFileName
     * 
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
     public function insertPostCodes($PostcodeAllocationID, $CSVFileName) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO postcode_allocation_data (PostcodeAllocationID, PostcodeArea)
        VALUES(:PostcodeAllocationID, :PostcodeArea)';

        $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

        $postCodesResult = $this->readCSVFile($CSVFileName);
        
        foreach($postCodesResult AS $pcKey=>$pcValue)
        {
            $insertQuery->execute(array(

                ':PostcodeAllocationID' => $PostcodeAllocationID,
                ':PostcodeArea' => $pcValue

                ));
        }
        
    }
     
     
     
     
     /**
     * Description
     * 
     * This method is used for to delete post codes for given PostcodeAllocationID.
     *  
     * @param int $PostcodeAllocationID
     *  
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function deletePostCodes($PostcodeAllocationID) {
        
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'DELETE FROM postcode_allocation_data WHERE PostcodeAllocationID=:PostcodeAllocationID';
        $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $deleteQuery->execute(array(':PostcodeAllocationID' => $PostcodeAllocationID));
        
    } 
     
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
   
    
    
}
?>